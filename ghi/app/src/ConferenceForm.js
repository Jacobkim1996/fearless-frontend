import React from 'react';

 class ConferenceForm extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            name: '',
            starts: '',
            ends: '',
            description: '',
            max_presentations: '',
            max_attendees: '',
            locations: [],
            location: '',
        };
        
        
        
        
        this.handleNameChange = this.handleNameChange.bind(this);
        this.handleStartsChange = this.handleStartsChange.bind(this);
        this.handleEndsChange = this.handleEndsChange.bind(this);
        this.handleDescriptionChange = this.handleDescriptionChange.bind(this);
        this.handleMaxPresentationsChange = this.handleMaxPresentationsChange.bind(this);
        this.handleMaxAttendeesChange = this.handleMaxAttendeesChange.bind(this);
        this.handleLocationChange = this.handleLocationChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);

    }
    async componentDidMount() {
        const url = 'http://localhost:8000/api/locations/'

        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            this.setState({locations: data.locations});
            
            }
        }

    async handleSubmit(event) {
        event.preventDefault();
        const data = {...this.location};
        delete data.locations;
        console.log(data);

        const conferenceUrl = 'http://localhost:8000/api/conferences/'
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const response = await fetch(conferenceUrl, fetchConfig);
        if (response.ok) {
            const newConference = await response.json();
            console.log(newConference);

            const cleared = {
                name: '',
                starts: '',
                ends: '',
                description: '',
                max_presentations: '',
                max_attendees: '',
                location: '',
            };
            this.setState(cleared);
        }
    }

    handleNameChange(event) {
        const value = event.target.value;
        this.setState({name: value})
    }

    handleStartsChange(event) {
        const value = event.target.value;
        this.setState({starts: value})
    }

    handleEndsChange(event){
        const value = event.target.value;
        this.setState({ends: value})
    }

    handleDescriptionChange(event){
        const value = event.target.value;
        this.setState({description: value})
    }

    handleMaxPresentationsChange(event){
        const value = event.target.value;
        this.setState({Max_presentations: value})
    }

    handleMaxAttendeesChange(event){
        const value = event.target.value;
        this.setState({Max_attendees: value})
    }

    handleLocationChange(event){
        const value = event.target.value;
        this.setState({Location: value})
    }
    
    render() {
        return (
            <div className="my-5 container">
                <div className="offset-3 col-6">
                    <div className="shadow p-4 mt-4">
                    <h1>Create a new conference</h1>
                    <form onSubmit={this.handleSubmit} id="create-conference-form">
                        <div className="form-floating mb-3">
                            <input value={this.state.name} onChange={this.handleNameChange} placeholder="Name" required type="text" name="name" id="name" className="form-control"/>
                            <label htmlFor="name">Name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input value={this.state.starts} onChange={this.handleStartsChange} placeholder="Starts" required type="date" name="starts" id="starts" className="form-control"/>
                            <label htmlFor="starts">Starts</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input value={this.state.ends} onChange={this.handleEndsChange} placeholder="Ends" required type="date" name="ends" id="ends" className="form-control"/>
                            <label htmlFor="ends">Ends</label>
                        </div>
                        <div className="mb-3">
                            <label htmlFor="exampleFormControlTextarea1" className="form-label">Description</label>
                            <textarea value={this.state.description} onChange={this.handleDescriptionChange} className="form-control" name="description" id="description" rows="3"></textarea>
                        </div>
                        <div className="form-floating mb-3">
                            <input value={this.state.max_presentations} onChange={this.handleMaxPresentationsChange} placeholder="Max presentations" required type="number" name="max_presentations" id="max_presentations" className="form-control"/>
                            <label htmlFor="max_presentations">Max presentations</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input value={this.state.max_attendees} onChange={this.handleMaxAttendeesChange} placeholder="Max_attendees" required type="number" name="max_attendees" id="max_attendees" className="form-control"/>
                            <label htmlFor="max_attendees">Max attendees</label>
                        </div>
                        <div className="mb-3">
                            <select value={this.state.location} onChange={this.handleLocationChange} required name="location" id="location" className="form-select">
                                <option value="">Choose a location</option>
                                {this.state.locations ? this.state.locations.map(location => {
                                    return (
                                        <option key={location.id} value={location.id}>
                                            {location.name}
                                        </option>
                                    );
                                }):null}
                            </select>    
                        </div>             
                        <button className="btn btn-primary">Create</button>
                    </form>
                    </div>
                </div>
            </div>
        
        );
    }
}

export default ConferenceForm;